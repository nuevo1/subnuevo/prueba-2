<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>2.input_use</name>
   <tag></tag>
   <elementGuidId>0493a089-180a-4e7c-ace5-e494e67208ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@class = 'select2-input' and @type = 'text' and @role = 'combobox' and @title = 'Search field']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;select2-drop&quot;]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
