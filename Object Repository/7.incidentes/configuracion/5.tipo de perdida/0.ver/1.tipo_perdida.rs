<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>1.tipo_perdida</name>
   <tag></tag>
   <elementGuidId>7acf3790-1fc8-4d8b-b38c-f1fe1cafd886</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/aside/div/section/ul/li[3]/ul/li[6]/ul/li[5]/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[@class = 'tq_self_none' and @href = '/app.php/staff/ohsas_loss_type?related_module=incident']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
