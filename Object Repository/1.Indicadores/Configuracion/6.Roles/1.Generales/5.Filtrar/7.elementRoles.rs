<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>7.elementRoles</name>
   <tag></tag>
   <elementGuidId>e1be206d-1ba8-4cdb-95ff-ec9aa15233cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[@class = 'sf_admin_text sf_admin_list_td_role_type_name']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sf_admin_text sf_admin_list_td_role_type_name</value>
   </webElementProperties>
</WebElementEntity>
