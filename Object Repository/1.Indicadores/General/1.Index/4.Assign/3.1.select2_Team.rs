<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>3.1.select2_Team</name>
   <tag></tag>
   <elementGuidId>bd00d918-942e-473f-a10b-4de50987a8bb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#select2-results-1 > li:nth-child(2)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#select2-results-1 > li:nth-child(2)</value>
   </webElementProperties>
</WebElementEntity>
