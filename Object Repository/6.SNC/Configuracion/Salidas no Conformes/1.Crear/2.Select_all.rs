<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>2.Select_all</name>
   <tag></tag>
   <elementGuidId>5e3286c1-1647-4b43-bd25-ec4c3dc31142</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#select2-drop > ul:nth-child(1) > li:nth-child(1)</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[8]/ul[1]/li[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
