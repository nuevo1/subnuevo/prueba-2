<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>6.btn_Reset</name>
   <tag></tag>
   <elementGuidId>b96ede94-4002-4b7a-b77e-6dcd5fd39350</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Restablecer')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[@class = 'btn-filter-reset btn btn-default btn-sm' and @href = '/app.php/staff/ap_var_impact/filter/action?_reset' and (text() = 'Restablecer' or . = 'Restablecer')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn-filter-reset btn btn-default btn-sm</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/app.php/staff/ap_var_impact/filter/action?_reset</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Restablecer</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;tablesaw-enhanced&quot;]/body[@class=&quot;skin-blue fixed sidebar-mini pace-done modal-open&quot;]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-filter&quot;]/div[@class=&quot;modal modal-filter in&quot;]/div[@class=&quot;modal-dialog ui-draggable&quot;]/div[@class=&quot;modal-content&quot;]/form[@class=&quot;sf_admin_filter_form form-horizontal&quot;]/div[@class=&quot;modal-footer&quot;]/a[@class=&quot;btn-filter-reset btn btn-default btn-sm&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Restablecer')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Breve descripción de la variable de imapcto'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Descripción'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lista de módulos'])[1]/preceding::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/app.php/staff/ap_var_impact/filter/action?_reset')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[3]/a</value>
   </webElementXpaths>
</WebElementEntity>
