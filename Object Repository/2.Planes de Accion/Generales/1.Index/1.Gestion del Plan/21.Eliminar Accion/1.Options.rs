<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>1.Options</name>
   <tag></tag>
   <elementGuidId>80608959-2e36-4bbe-a687-34694f487b3f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@class = 'dropdown-toggle btn btn-more btn-circle btn-sm' and @href = '#']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dropdown-toggle btn btn-more btn-circle btn-sm</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
   </webElementProperties>
</WebElementEntity>
