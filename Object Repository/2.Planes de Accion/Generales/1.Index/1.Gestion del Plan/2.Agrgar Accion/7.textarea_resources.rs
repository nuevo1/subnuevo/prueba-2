<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>7.textarea_resources</name>
   <tag></tag>
   <elementGuidId>5871a6d2-c5be-4332-9b70-2b3b841684eb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//textarea[@id = 'ap_action_resources' and @class = 'materialize-textarea  form-control']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ap_action_resources</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>materialize-textarea  form-control</value>
   </webElementProperties>
</WebElementEntity>
