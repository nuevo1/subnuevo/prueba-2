<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Iniciar Sesion</name>
   <tag></tag>
   <elementGuidId>51ed36be-0693-4ae0-949d-c5d0917e7de3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/section/div/div/div[2]/a[2][count(. | //*[@class = 'linkButtonSigninHeader']) = count(//*[@class = 'linkButtonSigninHeader'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>linkButtonSigninHeader</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/section/div/div/div[2]/a[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>a.linkButtonSigninHeader:nth-child(4)</value>
   </webElementProperties>
</WebElementEntity>
