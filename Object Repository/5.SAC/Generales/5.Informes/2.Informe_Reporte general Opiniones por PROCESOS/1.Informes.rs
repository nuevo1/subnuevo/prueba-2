<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>1.Informes</name>
   <tag></tag>
   <elementGuidId>dc2ba7f2-af3b-4b12-af7e-5e79b3bce88d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/aside/div/section/ul/li[3]/ul/li[5]/a
</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[@href = '/app.php/staff/wfi_opinion/indexReports' and @class = 'tq_self_none']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/app.php/staff/wfi_opinion/indexReports</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>tq_self_none</value>
   </webElementProperties>
</WebElementEntity>
