<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>4.Tipos de Roles_SAC</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5193b421-1763-4803-9013-8168e18a7b57</testSuiteGuid>
   <testCaseLink>
      <guid>513e12c6-a2f6-4f50-a1d7-20ab70c8849b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/4.Tipos de roles/1.Test Case Generales/0.Ver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00b36ec6-2409-4639-bcd8-60d96c2ee563</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/4.Tipos de roles/1.Test Case Generales/1.Crear</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a720b02-4388-4e61-b5d3-3b196913ce9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/4.Tipos de roles/1.Test Case Generales/2.Editar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96fc314b-1fa5-4d6c-ba24-8e3c2626516c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/4.Tipos de roles/1.Test Case Generales/3.Eliminar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eccf77ff-51bc-4333-bf1b-481acb717c1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/4.Tipos de roles/1.Test Case Generales/4.Inhabilitar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a1e968e7-6b1b-443f-bb38-96cfb8864504</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/4.Tipos de roles/1.Test Case Generales/5.Filtrar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6517a62e-f684-434f-9d59-e76169ee716e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/4.Tipos de roles/1.Test Case Generales/6.Desfiltrar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd7db8b8-8fb4-43ff-9a39-a818e7b98bab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/4.Tipos de roles/1.Test Case Generales/7.Mostrar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
