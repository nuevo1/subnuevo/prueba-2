<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>12.Temas_SAC</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1e015c61-f1b4-4e5a-a744-fbf48616cb29</testSuiteGuid>
   <testCaseLink>
      <guid>3e245928-e02f-427e-b9d5-bfafc1523dd6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/12.Temas/1.Test Case Generales/0.Ver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>279c1303-8400-4957-9fe1-ef223ba89372</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/12.Temas/1.Test Case Generales/1.Crear</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f21be44-e71f-4dd8-a99c-c12016db014c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/12.Temas/1.Test Case Generales/2.Editar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee3dc43b-18d3-43c2-a117-30fc6ee407f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/12.Temas/1.Test Case Generales/3.Eliminar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e26ab92-feff-4b03-8819-b70dcedace3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/12.Temas/1.Test Case Generales/4.Inhabilitar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7abb34fc-07d3-421f-ab97-3d95edf48b02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/12.Temas/1.Test Case Generales/5.Filtrar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>97e83f09-e93d-433f-8846-19ace172104e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/12.Temas/1.Test Case Generales/6.Desfiltrar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dbe2e7be-1bee-4be6-86b6-c78dc402c644</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/12.Temas/1.Test Case Generales/7.Mostrar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
