<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>5.Clases de Correcion_SAC</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a566b889-d385-4b33-8468-9e68a731f5b7</testSuiteGuid>
   <testCaseLink>
      <guid>7b7b2020-1c15-4f2b-8e12-76d368c9db07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/5.Clases de correcion/1.Test Case Generales/0.Ver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67d5be1a-f4d6-4b41-9497-a7d05d600349</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/5.Clases de correcion/1.Test Case Generales/1.Crear</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>826cbfea-a8d5-47d8-a4f1-7be4b3a96574</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/5.Clases de correcion/1.Test Case Generales/2.Editar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>69a856c2-3f45-49b7-af40-9e3ad58f5b80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/5.Clases de correcion/1.Test Case Generales/3.Eliminar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6cca97ef-8db8-4820-b6d4-d441f895b818</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/5.Clases de correcion/1.Test Case Generales/4.Inhabilitar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2f3def8-9e07-4ae9-b57d-82004355a628</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/5.Clases de correcion/1.Test Case Generales/5.Filtrar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d034dcea-e74c-48c4-804b-1b6c12817f85</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/5.Clases de correcion/1.Test Case Generales/6.Desfiltrar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac258fb2-3017-448c-b79d-11078fa26e31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.SAC/Configuracion/5.Clases de correcion/1.Test Case Generales/7.Mostrar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
