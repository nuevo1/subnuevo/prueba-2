<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>1.Test general Configuracion</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>80938fec-9825-4889-b9d0-07213bbd3f2c</testSuiteGuid>
   <testCaseLink>
      <guid>1555e229-2d0f-4a8b-b977-f9760deec2e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/1.Test General de Configuracion de Tipos</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41538172-a8a2-4b4d-b7cf-a776e9196b15</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/2.Test General de Configuracion de Origenes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b782060a-1c65-4d4a-b8d3-de169250087f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/3.Tipos de Accion/3.Test General de Confguracion de Tipos de Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de6def18-dee5-47df-96b6-93ab018c1b84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/4.Test General de Configuracion de Priorizacion de Causas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c828f3bd-9aa1-4bff-9949-3232a8b060e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/5.Test General de Configuracion de Impactos</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5e9cd23-8f06-4b29-aa3a-c12d3a09db18</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/6.Test General de Modelos de Gestion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b9e0f9d-28cb-4ff4-aa46-d1ec45f1c6c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/7.Test General de Variable de Impacto</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f5642d9-987d-41eb-a566-a74fce614cc2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/8.Test General de Nivel de Criticidad</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53a039e2-1962-4570-86df-59376ff68ab4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/9.Test General de Relaciones entre Variables de Impacto y Niveles de Criticidad</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
