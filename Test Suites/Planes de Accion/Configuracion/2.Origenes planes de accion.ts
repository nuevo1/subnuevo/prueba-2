<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>2.Origenes planes de accion</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>jsandoval@tiqal.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>be57803c-825c-4737-945e-d4b3600f0394</testSuiteGuid>
   <testCaseLink>
      <guid>62e6583d-51f9-45a7-8bb2-fe209b5d2105</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/2.Origenes planes de Accion/1.Test Cases General/0.Ver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>470edf18-ee5f-4f3b-ac98-f3a73706feef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/2.Origenes planes de Accion/1.Test Cases General/1.Crear</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>24a19f58-78c7-44b9-ab25-4c9f81a85dd7</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9a1d7c12-7050-4267-986c-86a348e701af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/2.Origenes planes de Accion/1.Test Cases General/2.Editar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43b88697-d0ba-4e22-9756-b43728d15e7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/2.Origenes planes de Accion/1.Test Cases General/3.Eliminar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5434c186-f400-4495-a456-70e2e79edab0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/2.Origenes planes de Accion/1.Test Cases General/4.Inhabilitar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26b22e7f-0503-4c57-b649-998fca763a01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/2.Origenes planes de Accion/1.Test Cases General/5.Filtrar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
