<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>3.Tipos de Accion</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>jsandoval@tiqal.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7d24daeb-3df0-49d2-8fc1-549c5c6f4c37</testSuiteGuid>
   <testCaseLink>
      <guid>0c405f62-5427-4fa6-817a-b6c8a9a16ebb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Test Cases General/0.Ver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ba27d4a-f46a-4341-bed0-dd588b7c9881</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Test Cases General/1.Crear</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38981f23-faac-4f21-b016-4964bac1c53f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Test Cases General/2.Editar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c8c567f-3a15-48fd-8cfc-08935edeaafb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Test Cases General/3.Eliminar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ca0dd48-bd12-4556-994c-74cc080d5d5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Test Cases General/4.Inhabilitar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29527b99-c157-4fcc-8c6e-1a1ab4e1d23a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Configuracion/3.Tipos de Accion/1.Test Cases General/5.Filtrar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
