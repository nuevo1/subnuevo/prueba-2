<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>30.concluir plan de accion desde tareas</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d7347233-8d36-47a8-9f81-ade532c4843f</testSuiteGuid>
   <testCaseLink>
      <guid>7b8ed19a-2162-4cf7-b761-6dc85a3d64b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.Crear Plan de Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf63fdb2-9859-458c-b161-2820797d558e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/2.Agregar Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85ea366c-ecaf-4ad4-aa80-1deb619822b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/6.Solicitar aprobacion del plan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48b61f67-e271-40c8-8bba-fa646ded51ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/29.aprobar acción desde la sección de tareas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>57df1326-98ad-4f04-8f38-9666a27f9ae3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/30.concluir plan de accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>445159d4-9764-4c4a-8fd1-635f9b2f1875</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/31.seguimiento plan de accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cfc23ffe-95a7-4fca-a7cb-2589e4300ab4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/32.concluir plan</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
