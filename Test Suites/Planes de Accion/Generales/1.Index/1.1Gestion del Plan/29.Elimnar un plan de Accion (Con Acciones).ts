<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>29.Elimnar un plan de Accion (Con Acciones)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>98b5e799-f869-42ac-bd8f-e70b3d139332</testSuiteGuid>
   <testCaseLink>
      <guid>04815535-d7c3-4ccb-9d07-237657294fc5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.Crear Plan de Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>067be911-b0c5-446e-9fbf-56ecfe14f60a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/2.Agregar Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c21372a-c58d-4447-94d0-114c827b0295</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/25.Eliminación de un plan de acción</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
