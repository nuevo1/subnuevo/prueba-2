<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>13.Concluir Plan de Accion</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b25b0bcd-a626-4b40-af60-199b59d32095</testSuiteGuid>
   <testCaseLink>
      <guid>38260404-316b-44bb-ab3a-3e4320e8c278</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.Crear Plan de Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>459aeb0b-ff7f-4d0f-9431-fc5ef4ad8956</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/2.Agregar Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6405dc8f-41c5-46c3-8ccc-ebd200394e1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/6.Solicitar aprobacion del plan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e71d4fe1-1d59-4a3d-9725-2487a3833d38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/7.Ejecutar Acciones</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36457308-878d-48e6-89d7-a771aa98687c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/8.Aprobar acciones</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac1b2278-5103-48ab-ae1a-2feeee8a6ca0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/16.Seguir Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>690fbfbe-88d6-4746-99a6-07925903b8f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/17.Concluir Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9de975cc-8aa4-4f88-a0f9-7fd7df373183</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/14.Seguimiento Accion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1ed9fe9-ceb5-426e-948a-e43513ccf438</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2.Planes de Accion/Generales/1.Index/1.1.Gestion del Plan/13.Concluir plan de accion</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
