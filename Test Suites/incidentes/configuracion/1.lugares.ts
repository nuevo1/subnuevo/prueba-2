<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>1.lugares</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c3970249-0734-406b-b6f7-c638f610e1f9</testSuiteGuid>
   <testCaseLink>
      <guid>56015ba4-a21f-4fe1-ab87-8add4b8a10d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/1.lugares/0.Ver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4eafaab8-b6db-45fa-81ad-b71f29b6e712</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/1.lugares/1.Crear</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c10d7c8-a6ca-453b-aa14-909bb4876e42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/1.lugares/2.Editar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be2dc438-d91c-4e00-bc96-6b21d26ae993</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/1.lugares/3.Eliminar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>970d7fa2-bb57-476f-be53-4cca920ae39e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/1.lugares/4.Inhabilitar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f8de6b81-0ddf-4c1d-907b-adfacfc9dbde</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/1.lugares/5.Filtrar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc9d3197-b126-4981-9af7-4ada51ee64e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/1.lugares/6.Mostrar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
