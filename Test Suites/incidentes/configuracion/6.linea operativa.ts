<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>6.linea operativa</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8b8d676e-cc9b-460d-93e0-79ee87d90f27</testSuiteGuid>
   <testCaseLink>
      <guid>31c6a59e-3cfa-4e7a-b15b-fd5864391779</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/6.linea operativa/0.Ver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53fe8642-b09a-4a22-96e7-4d1fdf4ba34e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/6.linea operativa/1.Crear</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e77a465c-4d0e-4737-b05b-98f37568dc15</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/6.linea operativa/2.Editar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>973cfe6b-52d8-4f8b-9b12-904a7505b8ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/6.linea operativa/3.Eliminar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d4f871d-a302-4438-8a86-ede671ef1e8f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/6.linea operativa/4.Inhabilitar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56900c37-e6f5-47af-977f-f2dc8b21bb84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/6.linea operativa/5.Filtrar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>483e7294-e84d-4dbc-b934-3651de8ab334</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/6.linea operativa/7.Mostrar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
