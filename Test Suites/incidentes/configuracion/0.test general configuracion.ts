<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>0.test general configuracion</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>cb6347b6-6b32-4833-9b66-61ec4b000f2d</testSuiteGuid>
   <testCaseLink>
      <guid>414baf3b-267d-4e3f-b01e-ae8ff3c0c18a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/1.test general configuracion lugares</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ab976c1-c4a2-460e-8d63-aab0dd4f444b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/2.test general configuracion criterio de prioritacion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7df5675d-31c7-4185-a905-01ce69fbfdda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/3.test general configuracion rango de priorizacion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b95d796a-c01b-4241-9acd-1ed8dfa9d111</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/4.test general configuracion clase de riegos</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d327d2c-8db3-48c2-8149-98fc7db2604c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/5.test general configuracion tipo de perdida</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a7d924f-e0d1-4c44-90b6-11e1513c7f29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/configuracion/6.test general configuracion linea operativa</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
