<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>2.gestion incidente - tareas</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0340c964-54f5-4895-97e9-d67822c8d1a0</testSuiteGuid>
   <testCaseLink>
      <guid>a7bf48b2-9825-4f27-a8d3-09eb590171d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/generales/1.index/1.crear con analisis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e65627f7-0937-43a8-aede-427ac87c6712</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/generales/1.index/1.1 gestion_incidentes/10.priorizar_incidentes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>473e1aee-bb22-4fb5-9b62-11dd1eb67758</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/generales/1.index/1.1 gestion_incidentes/6.analisis desde tareas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>631bc613-35aa-41af-9e97-1bc7c6646ddc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/generales/1.index/1.1 gestion_incidentes/7.correccion desde tareas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73bcbec6-1d2d-4400-9ee3-3e18de93b437</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/generales/1.index/1.1 gestion_incidentes/8.generar plan de accion tareas</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
