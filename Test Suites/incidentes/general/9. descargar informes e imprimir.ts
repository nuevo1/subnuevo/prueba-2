<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>9. descargar informes e imprimir</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>01415e10-e68c-42e6-8781-171ff12c45bc</testSuiteGuid>
   <testCaseLink>
      <guid>b5c85314-5b0b-4544-aa13-debee41b419f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/generales/4.informes/3. descargar_informe_PDF</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>341124f2-3ab1-4a52-a5a7-3e3bdbc04081</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/generales/4.informes/4. descargar_informe_excel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>771a1838-0ffd-4ba6-96ce-ba9e4c550b95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7.incidentes/generales/4.informes/5. imprimir_informe</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
