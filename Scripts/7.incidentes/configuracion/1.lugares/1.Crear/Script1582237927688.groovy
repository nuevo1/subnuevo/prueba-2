import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.callTestCase(findTestCase('7.incidentes/configuracion/1.lugares/0.Ver'), [:])

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/1.crear/1.btn_nuevo'))

WebUI.setText(findTestObject('7.incidentes/configuracion/1.lugares/1.crear/1.text_lugar'), palabra)

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/1.lugares/1.crear/1.text_area'), 'Esto es una descripcion')

WebUI.click(findTestObject('7.incidentes/configuracion/1.lugares/1.crear/1.btn_save'))

WebUI.delay(5)

