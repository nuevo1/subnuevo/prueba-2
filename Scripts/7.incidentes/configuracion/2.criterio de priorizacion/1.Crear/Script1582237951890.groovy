import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.callTestCase(findTestCase('7.incidentes/configuracion/2.criterio de priorizacion/0.Ver'), [:])

WebUI.click(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/1.btn_new'))

WebUI.delay(3)

WebUI.setText(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/1.text_critrio'), palabra)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/1.select'), 'HaEvalRequisite', 
    false)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/7.incidentes/configuracion/2.criterio de priorizacion/1.crear/1.text_area'), 
    'esto es una prueba')

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/2.select'), '50', 
    false)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/3.select'), '50', 
    false)

WebUI.delay(3)

WebUI.click(findTestObject('7.incidentes/configuracion/2.criterio de priorizacion/1.crear/1.btn_save'))

