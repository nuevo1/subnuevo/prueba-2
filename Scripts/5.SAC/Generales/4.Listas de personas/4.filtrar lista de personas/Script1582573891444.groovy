import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('5.SAC/Daruma/Acceso al modulo'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('5.SAC/Generales/4.Lista de personas/4.iltrar lista de personas/1.lista'))

WebUI.delay(3)

String name = CustomKeywords.'test.Metodos.get_Text'(findTestObject('5.SAC/Generales/4.Lista de personas/4.iltrar lista de personas/1.nombre'))

WebUI.delay(3)

WebUI.comment(name)

WebUI.click(findTestObject('5.SAC/Generales/4.Lista de personas/4.iltrar lista de personas/1.filter'))

WebUI.delay(3)

WebUI.setText(findTestObject('5.SAC/Generales/4.Lista de personas/4.iltrar lista de personas/1.input'), name)

WebUI.delay(3)

WebUI.click(findTestObject('5.SAC/Generales/4.Lista de personas/4.iltrar lista de personas/1.filtrar'))

WebUI.delay(3)

WebUI.click(findTestObject('5.SAC/Generales/4.Lista de personas/4.iltrar lista de personas/1.filter'))

WebUI.delay(3)

WebUI.click(findTestObject('5.SAC/Generales/4.Lista de personas/4.iltrar lista de personas/1.reset'))

