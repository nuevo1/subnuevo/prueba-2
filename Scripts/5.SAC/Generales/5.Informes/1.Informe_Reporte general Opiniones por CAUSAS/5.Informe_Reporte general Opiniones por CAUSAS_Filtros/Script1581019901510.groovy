import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('5.SAC/Daruma/Acceso al modulo'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/5.SAC/Generales/5.Informes/1.Informe_Reporte general Opiniones por TIPO/1.Informes'))

String num = CustomKeywords.'test.Metodos.get_numbers1'(findTestObject('5.SAC/Generales/5.Informes/4.Informe_Reporte general Opiniones por USUARIOS/1.Reporte General Por Usuarios'))

WebUI.click(findTestObject('5.SAC/Generales/5.Informes/4.Informe_Reporte general Opiniones por USUARIOS/1.Reporte General Por Usuarios'))

WebUI.switchToWindowUrl(num)

WebUI.selectOptionByValue(findTestObject('5.SAC/Generales/5.Informes/4.Informe_Reporte general Opiniones por USUARIOS/1.Filtro'), 
    'created_at', false)

WebUI.click(findTestObject('5.SAC/Generales/5.Informes/4.Informe_Reporte general Opiniones por USUARIOS/1.desde_fecha'))

WebUI.delay(5)

WebUI.click(findTestObject('5.SAC/Generales/5.Informes/4.Informe_Reporte general Opiniones por USUARIOS/1.1.desde_Fecha'))

WebUI.delay(5)

WebUI.click(findTestObject('5.SAC/Generales/5.Informes/4.Informe_Reporte general Opiniones por USUARIOS/1.Hasta_fecha'))

WebUI.delay(5)

WebUI.click(findTestObject('5.SAC/Generales/5.Informes/4.Informe_Reporte general Opiniones por USUARIOS/1.1.Hasta_Fecha'))

WebUI.delay(3)

WebUI.click(findTestObject('5.SAC/Generales/5.Informes/4.Informe_Reporte general Opiniones por USUARIOS/1.aplicar_filtros'))

WebUI.delay(5)

WebUI.click(findTestObject('5.SAC/Generales/5.Informes/4.Informe_Reporte general Opiniones por USUARIOS/1.Restablecer'))

WebUI.delay(10)

