import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('5.SAC/Configuracion/7.Clientes/1.Test Case Generales/0.Ver'), [:])

WebUI.delay(5)

CustomKeywords.'test.Metodos.lista'(findTestObject('5.SAC/Configuracion/7.Clientes/4.Inhabilitar/1.list_inputCheck'))

WebUI.click(findTestObject('5.SAC/Configuracion/7.Clientes/4.Inhabilitar/1Btn_Disable'))

WebUI.click(findTestObject('5.SAC/Configuracion/7.Clientes/4.Inhabilitar/2Btn_disable'))

WebUI.acceptAlert()

WebUI.delay(10)