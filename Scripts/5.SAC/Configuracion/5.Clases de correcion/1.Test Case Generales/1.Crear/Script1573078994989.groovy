import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.callTestCase(findTestCase('5.SAC/Configuracion/5.Clases de correcion/1.Test Case Generales/0.Ver'), [:])

WebUI.delay(5)

WebUI.click(findTestObject('5.SAC/Configuracion/5.Clases de correcion/1.Crear/create'))

WebUI.setText(findTestObject('5.SAC/Configuracion/5.Clases de correcion/1.Crear/input_Class'), palabra)

WebUI.setText(findTestObject('5.SAC/Configuracion/5.Clases de correcion/1.Crear/textarea_Descripcion'), 'descripcion clase prueba katalon')

WebUI.click(findTestObject('5.SAC/Configuracion/5.Clases de correcion/1.Crear/btn_save'))

WebUI.delay(10)