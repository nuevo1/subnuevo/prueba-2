import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.callTestCase(findTestCase('6.SNC/Configuracion/Salidas No Conformes/0.Ver'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('6.SNC/Configuracion/Salidas no Conformes/1.Crear/1.Crear'))

WebUI.setText(findTestObject('6.SNC/Configuracion/Salidas no Conformes/1.Crear/1.Name'), palabra)

CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.1.display_list_multi'), 0)

WebUI.delay(5)

WebUI.click(findTestObject('6.SNC/Configuracion/Salidas no Conformes/1.Crear/li_Seleccionar Todo'))

CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.1.display_list_multi'), 1)

WebUI.delay(5)

WebUI.click(findTestObject('6.SNC/Configuracion/Salidas no Conformes/1.Crear/li_Seleccionar Todo'))

WebUI.delay(5)

CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.1.display_list_multi'), 2)

WebUI.delay(5)

WebUI.click(findTestObject('6.SNC/Configuracion/Salidas no Conformes/1.Crear/li_Seleccionar Todo'))

WebUI.setText(findTestObject('6.SNC/Configuracion/Salidas no Conformes/1.Crear/3.Text_Area'), 'prueba katalon')

WebUI.delay(5)

WebUI.click(findTestObject('6.SNC/Configuracion/Salidas no Conformes/1.Crear/1.Save'))

WebUI.delay(10)

