import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
String palabra = CustomKeywords.'test.Metodos.randomString'()
String palabra1 = CustomKeywords.'test.Metodos.randomStringEdit'()
//0.Ver
WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/0.Ver/1.Configuration'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/0.Ver/2.Types'))

WebUI.delay(5)

//1.Crear Flujo completo
WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/1.btn_Create'))

WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/2.input_name'),
palabra)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/3.inputCheck_has_impact'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/4.input_planning_days'), '90')

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/5.input_approved_days'), '30')

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/6.input_closure_days'), '60')

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/7.inputCheck_auto_notify'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/8.inputCheck_all_signatures'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/9.inputCheck_manual_advance'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/10. checkAll_disabled_fields'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/11.textarea_notes'), 'Este flujo lo tiene todo...')

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/12.btn_Save'))

WebUI.delay(5)

//Volver
WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Test Cases General/Volver Tipos'))

WebUI.delay(5)

//2.Edit
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/2.btn_Edit'))

WebUI.delay(5)

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/3.input_name'), palabra1)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/4.inputCheck_has_impact'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/5.inputCheck_has_analysis'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/6.inputCheck_has_budget'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/7.input_planning_days'), '100')

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/8.input_approved_days'), '50')

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/9.input_closure_days'), '75')

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/10.inputCheck_manual_advance'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/11.inputCheck_all_signatures'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/12.clearAll_disable_fields'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/2.Editar/13.btn_Save'))

WebUI.delay(5)

//Volver
WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Test Cases General/Volver Tipos'))

WebUI.delay(5)

//3.Delete
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/3.Eliminar/2.btn_Delete'))

WebUI.acceptAlert()

WebUI.delay(5)

//4.Disable
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/4.Inhabilitar/2.btn_Disable_1'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/4.Inhabilitar/3.btn_Disable_2'))

WebUI.acceptAlert()

WebUI.delay(5)

//5.Filtrar
WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/5.Filtrar/1.btn_Filter'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/5.Filtrar/2.input_filtersnametext'),
	'Plan Reactivo')

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/5.Filtrar/3.select_has_budget'),
	'1', true)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/5.Filtrar/4.select_has_analysis'),
	'1', true)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/5.Filtrar/5.select_state'),
	'1', true)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/5.Filtrar/6.input_filtersnotesis_empty'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/5.Filtrar/7.btn_filter'))

WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/5.Filtrar/1.btn_Filter'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Configuracion/1.Tipos/1.Generales/5.Filtrar/8.btn_Reset'))
