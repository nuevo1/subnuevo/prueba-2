import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String palabra = CustomKeywords.'test.Metodos.randomStringEdit'()

WebUI.callTestCase(findTestCase('2.Planes de Accion/Configuracion/5.Impactos/1.Test Cases General/0.Ver'), [:])

//WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/2.Editar/1.inputCheck_items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/2.Editar/2.btn_Edit'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/2.Editar/3.input_impactname'), palabra)

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/2.Editar/4.textarea_impactnotes'), 
    'Esto es una Edicion de Flujo 1')

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/2.Editar/5.btn_Save'))

WebUI.delay(5)

