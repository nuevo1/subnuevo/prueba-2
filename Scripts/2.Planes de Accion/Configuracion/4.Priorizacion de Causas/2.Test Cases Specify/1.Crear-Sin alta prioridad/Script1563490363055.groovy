import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Configuracion/4.Priorizacion de Causas/1.Test Cases General/0.Ver'),
	[:])

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/4.Priorizacion de Causas/1.Generales/1.Crear/1.btn_create'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/4.Priorizacion de Causas/1.Generales/1.Crear/2.input_effectname'),
	'Flujo 2')

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/4.Priorizacion de Causas/1.Generales/1.Crear/4.textarea_effectnotes'),
	'Esto es una descripcion')

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/4.Priorizacion de Causas/1.Generales/1.Crear/5.btn_Save'))

WebUI.delay(5)