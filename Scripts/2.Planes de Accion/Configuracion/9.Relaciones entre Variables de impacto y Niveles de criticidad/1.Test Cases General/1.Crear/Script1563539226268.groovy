import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


WebUI.callTestCase(findTestCase('2.Planes de Accion/Configuracion/9.Relaciones entre Variables de impacto y Niveles de criticidad/1.Test Cases General/0.Ver'), 
    [:])

//1.Crear

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/9.Relacion entre Variables de Impacto y Nivel de Criticidad/1.Generales/1.Crear/1.btn_Create'))

int element = CustomKeywords.'test.Metodos.select2_config'(findTestObject('2.Planes de Accion/Configuracion/9.Relacion entre Variables de Impacto y Nivel de Criticidad/1.Generales/1.Crear/2.select_variable_impact'))

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Configuracion/9.Relacion entre Variables de Impacto y Nivel de Criticidad/1.Generales/1.Crear/2.select_variable_impact'),element.toString(), false)

int element2 = CustomKeywords.'test.Metodos.select2_config'(findTestObject('2.Planes de Accion/Configuracion/9.Relacion entre Variables de Impacto y Nivel de Criticidad/1.Generales/1.Crear/3.select_criticality_level'))

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Configuracion/9.Relacion entre Variables de Impacto y Nivel de Criticidad/1.Generales/1.Crear/3.select_criticality_level'),element2.toString(), false)

//TinyMCE Obligatorio
CustomKeywords.'test.Metodos.EdiTtiny1'(findTestObject('Object Repository/2.Planes de Accion/Configuracion/9.Relacion entre Variables de Impacto y Nivel de Criticidad/1.Generales/Iframe TinyMCE'),0)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/9.Relacion entre Variables de Impacto y Nivel de Criticidad/1.Generales/1.Crear/4.btn_Save'))

WebUI.delay(5)

