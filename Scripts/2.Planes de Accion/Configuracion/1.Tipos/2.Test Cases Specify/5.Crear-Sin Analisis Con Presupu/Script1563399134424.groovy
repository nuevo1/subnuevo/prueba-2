import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Configuracion/1.Tipos/1.Test Cases Generales/0.Ver'), [:])

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/1.btn_Create'))

WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/2.input_name'),
	'Flujo 6')

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/15.inputCheck_has_analysis'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/3.inputCheck_has_impact'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/4.input_planning_days'), '90')

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/5.input_approved_days'), '30')

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/6.input_closure_days'), '60')

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/7.inputCheck_auto_notify'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/8.inputCheck_all_signatures'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/9.inputCheck_manual_advance'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/11.textarea_notes'), 'Este flujo lo tiene todo...')

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/1.Tipos/1.Generales/1.Crear/12.btn_Save'))