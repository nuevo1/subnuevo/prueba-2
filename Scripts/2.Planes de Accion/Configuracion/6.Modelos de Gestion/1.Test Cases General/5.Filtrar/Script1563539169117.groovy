import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Configuracion/6.Modelos de Gestion/1.Test Cases General/0.Ver'), [:])

//5.Filtrar
WebUI.click(findTestObject('2.Planes de Accion/Configuracion/6.Modelos de Gestion/1.Generales/5.Filtrar/1.open_filter'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/6.Modelos de Gestion/1.Generales/5.Filtrar/2.input_model_filtersnametext'),
	'Edicion de Flujo 1')

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Configuracion/6.Modelos de Gestion/1.Generales/5.Filtrar/3.select_model_filtersstate'),
	'1', true)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/6.Modelos de Gestion/1.Generales/5.Filtrar/4.inputCheck_model_filtersnotesis_empty'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/6.Modelos de Gestion/1.Generales/5.Filtrar/5.btn_filter'))

WebUI.delay(8)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/6.Modelos de Gestion/1.Generales/5.Filtrar/1.open_filter'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/6.Modelos de Gestion/1.Generales/5.Filtrar/6.btn_Reset'))