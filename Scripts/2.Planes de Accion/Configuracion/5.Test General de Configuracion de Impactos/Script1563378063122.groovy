import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/0.Ver/1.a_Configuration'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/0.Ver/2.a_Impactos'))

WebUI.delay(5)

//1.Crear
WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/1.Crear/1.btn_Create'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/1.Crear/2.input_impactname'), 'Flujo 1')

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/1.Crear/3.textarea_impactnotes'), 
    'Esto es una Descripcion...')

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/1.Crear/4.btn_Save'))

WebUI.delay(5)

//Volver
WebUI.click(findTestObject('2.Planes de Accion/Test Cases General/Volver Impactos'))

WebUI.delay(5)

//2.Editar
//WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/2.Editar/1.inputCheck_items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/2.Editar/2.btn_Edit'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/2.Editar/3.input_impactname'), 'Editando Flujo 1')

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/2.Editar/4.textarea_impactnotes'), 
    'Esto es una Edicion de Flujo 1')

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/2.Editar/5.btn_Save'))

WebUI.delay(5)

//Volver
WebUI.click(findTestObject('2.Planes de Accion/Test Cases General/Volver Impactos'))

WebUI.delay(5)

//3.Eliminar
//WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/3.Eliminar/1.inputCheck_items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/3.Eliminar/2.btn_Delete'))

WebUI.acceptAlert()

WebUI.delay(5)

//4.Inhabilitar
//WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/4.Inhabilitar/1.inputCheck_items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/4.Inhabilitar/2.btn_Disable_1'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/4.Inhabilitar/3.btn_Disable_2'))

WebUI.acceptAlert()

WebUI.delay(5)

//5.Filtrar
WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/5.Filtrar/1.open_filter'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/5.Filtrar/2.input_filtersnametext'), 
    'Legal')

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/5.Filtrar/3.input_filtersnotesis_empty'))

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/5.Filtrar/4.select_impact_filters_state'), 
    '1', true)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/5.Filtrar/5.btn_filter'))

WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/5.Filtrar/1.open_filter'))

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/5.Impactos/1.Generales/5.Filtrar/6.btn_Reset'))

