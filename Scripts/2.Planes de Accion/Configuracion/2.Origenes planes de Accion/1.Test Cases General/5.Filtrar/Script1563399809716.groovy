import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Configuracion/2.Origenes planes de Accion/1.Test Cases General/0.Ver'), 
    [:])

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Configuracion/2.Origenes planes de Accion/1.Generales/5.Filtrar/1.btn_Filter'))

WebUI.setText(findTestObject('2.Planes de Accion/Configuracion/2.Origenes planes de Accion/1.Generales/5.Filtrar/2.input_filtersnametext'), 
    'Editando ando')

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Configuracion/2.Origenes planes de Accion/1.Generales/5.Filtrar/3.select_related_record'), 
    'AccidentBase', true)

WebUI.click(findTestObject('2.Planes de Accion/Configuracion/2.Origenes planes de Accion/1.Generales/5.Filtrar/4.btn_Filter'))

WebUI.delay(5)
