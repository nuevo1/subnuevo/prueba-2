import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

int id = CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

CustomKeywords.'test.Metodos.select_item_managment'(findTestObject('Generales/Select Item List/1.list_index'), id)
//WebUI.click(findTestObject('Generales/Select Item List/1.list_index'))

WebUI.delay(5)

//Analisis...
WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/1.btn_option_more'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/2.btn_analyze'))

CustomKeywords.'test.Metodos.tiny1'(findTestObject('Object Repository/Generales/TinyMCE/Iframe'),0)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/3.btn_Save'))

WebUI.delay(5)

//Clasificacion
WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/4.select_cause_effect'),'2', false)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/5.inputCheck_add_actions'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/3.btn_Save'))

//Acciones
CustomKeywords.'test.Metodos.tiny1'(findTestObject('Object Repository/Generales/TinyMCE/Iframe'),0)
 
WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/6.select_action_type'),'1', true)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/7.select_end_date_month'),'11', true)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/8.select_end_date_day'),'31', true)

//WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/9.input_initial_budget'),'5000000')

//WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/10.input_investment_budget'),'5100000')

//WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/11.input_operating_budget'),'4800000')

//WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/12.inputCheck_exclude_consolidate'))


WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/1.Analisis/3.btn_Save'))

