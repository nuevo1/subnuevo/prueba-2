import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

int id = CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

CustomKeywords.'test.Metodos.select_item_managment'(findTestObject('Generales/Select Item List/1.list_index'), id)

//WebUI.click(findTestObject('Generales/Select Item List/1.list_index'))
WebUI.delay(8)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/6.Solicitar Plan de Accion/1.btn_option_more'))

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/6.Solicitar Plan de Accion/2.btn_ask_for_approval'))

WebUI.acceptAlert()



