import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

int id = CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

CustomKeywords.'test.Metodos.select_item_managment'(findTestObject('Generales/Select Item List/1.list_index'), id)

//WebUI.click(findTestObject('Generales/Select Item List/1.list_index'))
WebUI.delay(8)

String numActiosn = CustomKeywords.'test.Metodos.get_numbers'(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/4.Reasignar Acciones/1.elementContent_NumbersActions'))

WebUI.comment(numActiosn)
int num = Integer.parseInt(numActiosn)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/4.Reasignar Acciones/2.btn_option_more'))

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/4.Reasignar Acciones/3.btn_reassignActions'))

WebUI.delay(5)

for(int i=0; i < num; i++ ){
	
	CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Object Repository/Generales/Select2/1.display_list_only'), i)
	
	CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)
}

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/4.Reasignar Acciones/4.btn_Save'))
