import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/27.Validacion de formulario Plan de Accion/12.List_All'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/1.btn_Create'))

WebUI.delay(5)

//Proceso Origen

//Area Origen

//Proyectos
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'),
	0)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Origenes


//Registro


//Tipo

WebUI.getNumberOfTotalOption(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/4.Element_select'))

//Add Custom Keyword for TinyMCE Description of the action plan


WebUI.delay(5)

//Add Custom Keyword for TinyMCE Closing goal


WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/7.select_track_freq'), '4', true)

//cantidad periodos

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/9.select_criticality_id'), '21',
	true)

//Responsables
//Lider
//CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 0)


//Responsable de Seguimiento

//Procesos Asociados
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'),
	5)

WebUI.delay(5)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)
//Lista de Areas
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'),
6)
CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Sistemas de Gestion
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'),
	7)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Modelos
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'),
	8)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/10.btn_Save'))