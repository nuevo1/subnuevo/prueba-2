import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.callTestCase(findTestCase('Login'), [:])

WebUI.delay(5)

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/29.aprobar acción desde la sección de tareas/1.Task'))

WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/29.aprobar acción desde la sección de tareas/1.List_task_action_plan'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/29.aprobar acción desde la sección de tareas/1.accion_name'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/32.concluir plan/1.input_check'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/32.concluir plan/2.input_check'))

WebUI.delay(5)

CustomKeywords.'test.Metodos.tiny1'(findTestObject('2.Planes de Accion/Generales/1.Index/1.1.Crear/6.TinyMCE'), 0)

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/32.concluir plan/1.text_area'), 
    palabra)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/1.Gestion del Plan/32.concluir plan/1.btn_save'))

WebUI.delay(5)

