import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

WebUI.delay(10)

CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('Object Repository/2.Planes de Accion/Generales/1.Index/2.Editar/0.btn_Edit'))

WebUI.delay(15)

//Proceso Origen
CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 1)

CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Area Origen
CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 2)

CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Proyectos
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'),3)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/2.Editar/2.inputCheck_origins'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/2.Editar/3.inputCheck_origins'))

//Registro
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    1)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.1.select_option_multi'), 2)

//Tipo
int elementSelect = CustomKeywords.'test.Metodos.select2_config'(findTestObject('2.Planes de Accion/Generales/1.Index/2.Editar/4.Element_select'))

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/2.Editar/5.select_type'), elementSelect.toString(), 
    true)

//Add Custom Keyword for TinyMCE Description of the action plan
CustomKeywords.'test.Metodos.tiny1'(findTestObject('2.Planes de Accion/Generales/1.Index/2.Editar/6.TinyMCE'))

WebUI.delay(5)

//Add Custom Keyword for TinyMCE Closing goal
CustomKeywords.'test.Metodos.tiny2'(findTestObject('2.Planes de Accion/Generales/1.Index/2.Editar/6.TinyMCE'))

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/2.Editar/7.select_track_freq'), '3', true)

WebUI.setText(findTestObject('2.Planes de Accion/Generales/1.Index/2.Editar/8.input_num_periods'), '2')

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/1.Index/2.Editar/9.select_criticality_id'), '1', 
    true)

//Responsables
//Lider
//CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 2)

//CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)

//Encargado de Aprobar
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    2)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)

//Responsable de Seguimiento
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    3)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)

//Responsable de Seguimiento de Efectividad
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    4)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)

//Procesos Asociados
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    5)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)

//Lista de Areas
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    6)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)

//Sistemas de Gestion
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    7)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)

//Modelos
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    8)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)

WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Generales/1.Index/2.Editar/10.btn_Save'))

