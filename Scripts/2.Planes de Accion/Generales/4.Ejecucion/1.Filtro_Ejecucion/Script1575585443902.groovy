import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

WebUI.delay(6)

WebUI.click(findTestObject('2.Planes de Accion/Daruma/Acceso al modulo/4.Open_Ejecution'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/1filter'))

int elementSelect1 = CustomKeywords.'test.Metodos.select2_config'(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/2select_type_plan'))

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/2select_type_plan'), elementSelect1.toString(), 
    true)

//WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/2select_type_plan'), '1', true)
//WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/3Select_Origin'), '11', true)
int elementSelect = CustomKeywords.'test.Metodos.select2_config'(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/3Select_Origin'))

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/3Select_Origin'), elementSelect.toString(), 
    true)

WebUI.setText(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/4input_Lider'), 'fredy sandoval')

WebUI.setText(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/5input_responsible'), 'fredy sandoval')

WebUI.setText(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/6input_filtername'), 'prueba')

WebUI.setText(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/7input_filternotes'), 'prueba')

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/8select_financiacion'), '11', true)

WebUI.setText(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/9input_Area_Origen'), 'prueba')

WebUI.setText(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/10input_filterprocess'), 'prueba origen')

WebUI.setText(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/9input_Area_Origen'), 'prueba area origen')

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/11input_state'), '4', true)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/12select_Si_no'), '1', true)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/13select_year'), '2019', true)

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/14select_sistema_gestion'), '23', true)

WebUI.delay(5)

WebUI.click(findTestObject('2.Planes de Accion/Generales/3.Ejecucion/15input_btn_save'))

