import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

WebUI.delay(6)

WebUI.click(findTestObject('2.Planes de Accion/Daruma/Acceso al modulo/5.Open_Reports'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/4.Informes/1.Informes por acciones_filtro/1.General_informes'))

WebUI.switchToWindowUrl('http://52.11.126.234/app.php/staff/actionplan/viewReportByGeneral')

WebUI.delay(6)

WebUI.click(findTestObject('2.Planes de Accion/Generales/4.Informes/4.Informes_Reporte general por acciones_Imprimir/1.Print'))

