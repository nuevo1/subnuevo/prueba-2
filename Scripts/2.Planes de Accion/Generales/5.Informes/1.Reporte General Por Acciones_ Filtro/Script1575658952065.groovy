import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('2.Planes de Accion/Daruma/Acceso al modulo'), [:])

WebUI.delay(6)

WebUI.click(findTestObject('2.Planes de Accion/Daruma/Acceso al modulo/5.Open_Reports'))

WebUI.click(findTestObject('2.Planes de Accion/Generales/4.Informes/1.Informes por acciones_filtro/1.General_informes'))

WebUI.switchToWindowUrl('http://52.11.126.234/app.php/staff/actionplan/viewReportByGeneral')

WebUI.delay(6)

WebUI.click(findTestObject('2.Planes de Accion/Generales/4.Informes/1.Informes por acciones_filtro/2.filter'))

WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/4.Informes/1.Informes por acciones_filtro/1select_Correctiva'), 
    '1', true)
// agregar la descripcion a filtrar 
//WebUI.setText(findTestObject('2.Planes de Accion/Generales/4.Informes/1.Informes por acciones_filtro/2input_txt'), 'FREDY SANDOVAL')

//WebUI.setText(findTestObject('2.Planes de Accion/Generales/4.Informes/1.Informes por acciones_filtro/3input_txt'), 'PRUEBA')

// valores de los id : 4:ejecucion,  5:finalizado, 0:anulado, 1:pendiente por aceptar, 8:pendeinte por ajustar, 7:pendiente por aprobar,  10:pendiente por cerrar, 2:planeacion, 3:pendeinte por ejecutar, 9: seguimiento
WebUI.selectOptionByValue(findTestObject('2.Planes de Accion/Generales/4.Informes/1.Informes por acciones_filtro/4select_State'), 
    '6', true)

WebUI.click(findTestObject('2.Planes de Accion/Generales/4.Informes/1.Informes por acciones_filtro/5input_btn_save'))

