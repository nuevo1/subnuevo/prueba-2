import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.callTestCase(findTestCase('1.Indicadores/Daruma/Acceso al modulo'), [:])

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/1.btn_Create'))

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/1.Create/2.select_teams_list'),
	'11', true)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/3.select2_Team'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/1.Indicadores/General/1.Index/1.Create/3.1.select2_Team'))

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/4.input_basename'), palabra)

WebUI.comment('Despues de este input hay un TinyMCE, utilizar Javascript para llenarlo o utilizar el metodo de las Keywords')

WebUI.executeJavaScript('tinyMCE.activeEditor.setContent(\'Texto melo...\',{format : \'raw\'})', [])

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/5.input_measurement_unit'), '%')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/1.Create/6.select_measurement_at_month'), '2', true)

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/1.Create/7.select_measurement_at_day'), '22', true)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/13.inputCheck_is_public'))

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/1.Create/14.select_Category'), '1', true)

//Lista de Procesos
CustomKeywords.'test.Metodos.desplegar_lista_select2'(findTestObject('Object Repository/1.Indicadores/General/1.Index/1.Create/16.select2_processes_list'), 0)

WebUI.delay(5)

CustomKeywords.'test.Metodos.select2'(findTestObject('1.Indicadores/General/1.Index/1.Create/17.select2_processes_list'),1)

WebUI.delay(5)

//WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/18.select2_project_list'))
//Proyectos
CustomKeywords.'test.Metodos.desplegar_lista_select2'(findTestObject('Object Repository/1.Indicadores/General/1.Index/1.Create/16.select2_processes_list'), 1)

WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/17.select2_processes_list'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/19.inputCheck_uses_num_and_den'))

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/20.input_numerator_notes'),
	'num')

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/21.input_denominator_notes'),
	'dem')

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/22.input_is_inductor'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/23.btn_Save'))