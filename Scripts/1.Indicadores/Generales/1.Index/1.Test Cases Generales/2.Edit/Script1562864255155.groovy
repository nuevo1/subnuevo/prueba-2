import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.callTestCase(findTestCase('1.Indicadores/Daruma/Acceso al modulo'), [:])

WebUI.delay(10)

//WebUI.click(findTestObject('1.Indicadores/General/1.Index/2.Edit/1.inputCheck_Items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('1.Indicadores/General/6.Equipos/0.Ver/2.list_inputCheck'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/2.Edit/2.btn_Edit'))

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/2.Edit/3.select_indicator_type'), '1', true)

WebUI.delay(10)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/2.Edit/4.remove_Item_Team'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/2.Edit/4.1.select2_Team'))

WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/2.Edit/4.2.select2_Team'))

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/2.Edit/5.input__basename'), palabra)

WebUI.executeJavaScript('tinyMCE.activeEditor.setContent(\'Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo eget malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.\',{format : \'raw\'})', 
    [])

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/2.Edit/6.input_measurement_unit'), 'dias')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/2.Edit/7.select_measurement_at_month'), '4', true)

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/2.Edit/8.select_measurement_at_day'), '25', true)

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/2.Edit/9.input_measurement_frequency'), '2')

WebUI.click(findTestObject('1.Indicadores/General/1.Index/2.Edit/10.inputCheck_is_public'))

//Si Katalon quita los dos se renombran y utilizan, sino se busca una solucion
WebUI.click(findTestObject('1.Indicadores/General/1.Index/2.Edit/11.Remove_Item_processes_list'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/2.Edit/12.remove_Item_project_list'))

WebUI.delay(3)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/2.Edit/14.select2_processes_list'))

WebUI.delay(3)

CustomKeywords.'test.Metodos.select2'(findTestObject('1.Indicadores/General/1.Index/2.Edit/14.1.select2_Items'),0)

WebUI.delay(3)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/2.Edit/15.select2_project_list'))

WebUI.delay(3)

CustomKeywords.'test.Metodos.select2'(findTestObject('1.Indicadores/General/1.Index/2.Edit/14.1.select2_Items'), 0)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/2.Edit/16.btn_Save'))



