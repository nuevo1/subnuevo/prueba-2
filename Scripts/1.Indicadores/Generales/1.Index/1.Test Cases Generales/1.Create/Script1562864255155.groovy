import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.After
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.callTestCase(findTestCase('1.Indicadores/Daruma/Acceso al modulo'), [:])

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/1.btn_Create'))

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/1.Create/2.select_teams_list'), 
    '12', true)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/3.select2_Team'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/1.Indicadores/General/1.Index/1.Create/3.1.select2_Team'))

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/4.input_basename'), palabra)

WebUI.comment('Despues de este input hay un TinyMCE, utilizar Javascript para llenarlo o utilizar el metodo de las Keywords')

WebUI.executeJavaScript('tinyMCE.activeEditor.setContent(\'Texto melo...\',{format : \'raw\'})', [])

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/5.input_measurement_unit'), '%')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/1.Create/6.select_measurement_at_month'), '2', true)

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/1.Create/7.select_measurement_at_day'), '22', true)

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/8.input_str1'),
	'100')

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/9.input_str2'),
	'Cali')

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/10.input_str3'),
	'Cristian Henao')

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/11.textarea_txt1'),
	'Zonas ara indicadores')

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/12.textarea_txt2'),
	'Fuentes raices')

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/13.inputCheck_is_public'))

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/1.Create/14.select_Category'), '1', true)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/15.select_all_indicator_classes_list'))

WebUI.comment('Aqui hay un Select2 de Opcion Multiple, para esto es necesario hacer lo siguiente.')

WebUI.comment('1.Desplegar la lista del select2')

//Lista de Procesos

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/1.Indicadores/General/1.Index/1.Create/16.select2_processes_list'), 0)

WebUI.delay(5)

CustomKeywords.'test.Metodos.select2'(findTestObject('1.Indicadores/General/1.Index/1.Create/17.select2_processes_list'),1)

WebUI.delay(5)

//Proyectos
CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/1.Indicadores/General/1.Index/1.Create/16.select2_processes_list'), 1)

WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/17.select2_processes_list'))

WebUI.delay(5)

//Lista de Tareas
CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/1.Indicadores/General/1.Index/1.Create/16.select2_processes_list'), 2)

WebUI.delay(5)

CustomKeywords.'test.Metodos.select2'(findTestObject('1.Indicadores/General/1.Index/1.Create/17.select2_processes_list'),1)

//Fallas Comunes
CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/1.Indicadores/General/1.Index/1.Create/16.select2_processes_list'), 3)

WebUI.delay(5)

CustomKeywords.'test.Metodos.select2'(findTestObject('1.Indicadores/General/1.Index/1.Create/17.select2_processes_list'), 0)

//Lista de Documentos
CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/1.Indicadores/General/1.Index/1.Create/16.select2_processes_list'), 4)

WebUI.delay(5)

CustomKeywords.'test.Metodos.select2'(findTestObject('1.Indicadores/General/1.Index/1.Create/17.select2_processes_list'),1)

//Lista de Servicios
CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/1.Indicadores/General/1.Index/1.Create/16.select2_processes_list'), 5)

WebUI.delay(5)

CustomKeywords.'test.Metodos.select2'(findTestObject('1.Indicadores/General/1.Index/1.Create/17.select2_processes_list'),0)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/19.inputCheck_uses_num_and_den'))

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/20.input_numerator_notes'),
	'num')

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/1.Create/21.input_denominator_notes'),
	'dem')

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/22.input_is_inductor'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/1.Create/23.btn_Save'))

