import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('1.Indicadores/Daruma/Acceso al modulo'), [:])

WebUI.delay(5)

//WebUI.click(findTestObject('1.Indicadores/General/1.Index/3.Feed Indicator/1.inputCheck_Items'))

CustomKeywords.'test.Metodos.lista'(findTestObject('1.Indicadores/General/6.Equipos/0.Ver/2.list_inputCheck'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/3.Feed Indicator/2.btn_Feed'))

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/3.Feed Indicator/3.input_indicator_value'), '15')

WebUI.delay(5)

WebUI.executeJavaScript('tinyMCE.activeEditor.setContent(\'Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo eget malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.\',{format : \'raw\'})', 
    [])

WebUI.click(findTestObject('1.Indicadores/General/1.Index/3.Feed Indicator/4.btn_Save'))

