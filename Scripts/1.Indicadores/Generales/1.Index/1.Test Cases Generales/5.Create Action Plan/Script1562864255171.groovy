import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebDriverException as WebDriverException
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait as WebDriverWait
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('1.Indicadores/Daruma/Acceso al modulo'), [:])

//WebUI.click(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/1.inputCheck_Item'))
CustomKeywords.'test.Metodos.lista'(findTestObject('1.Indicadores/General/6.Equipos/0.Ver/2.list_inputCheck'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/2.btn_indicator_options'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/3.a_create_action_plan'))

//Proceso Origen 
CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 0)

CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Area Origen
CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 1)

CustomKeywords.'test.Metodos.element_selected'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 1)

//Proyectos
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    0)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Origenes
WebUI.click(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/4.inputCheck_origins'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/5.inputCheck_origins'))

//Registro
//Aniadirlo....
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    1)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.1.select_option_multi'), 0)

//Definicion
//Tipo
int elementSelect = CustomKeywords.'test.Metodos.select2_config'(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/6.Element_select'))

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/6.1.select_type'), elementSelect.toString(), 
    true)

//Add Custom Keyword for TinyMCE Description of the action plan
CustomKeywords.'test.Metodos.tiny1'(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/7.TinyMCE'))

WebUI.delay(5)

//Add Custom Keyword for TinyMCE Closing goal
CustomKeywords.'test.Metodos.tiny2'(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/7.TinyMCE'))

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/8.select_track_freq'), '3', 
    true)

WebUI.setText(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/9.input_num_periods'), '2')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/10.select_criticality_id'), 
    '1', true)

//Responsables
//Lider
CustomKeywords.'test.Metodos.display_list_only'(findTestObject('Generales/Select2/1.display_list_only'), 2)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Encargado de Aprobar
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    2)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Responsable de Seguimiento
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    3)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Responsable de Seguimiento de Efectividad
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    4)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Procesos Asociados
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    5)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Lista de Areas
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    6)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Sistemas de Gestion
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    7)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

//Modelos
CustomKeywords.'test.Metodos.display_list_multi'(findTestObject('Object Repository/Generales/Select2/1.1.display_list_multi'), 
    8)

CustomKeywords.'test.Metodos.select2'(findTestObject('Object Repository/Generales/Select2/2.select_option'), 0)

WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/5.Create Action Plan/11.btn_Save'))

