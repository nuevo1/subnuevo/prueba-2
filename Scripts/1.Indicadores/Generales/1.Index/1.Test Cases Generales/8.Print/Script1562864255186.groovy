import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('1.Indicadores/Daruma/Acceso al modulo'), [:])

//WebUI.click(findTestObject('1.Indicadores/General/1.Index/8.Print/1.inputCheck_Items'))
WebUI.delay(5)

int id = CustomKeywords.'test.Metodos.lista'(findTestObject('1.Indicadores/General/6.Equipos/0.Ver/2.list_inputCheck'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/8.Print/2.btn_Indicator_Options'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/8.Print/3.a_Print'))

WebUI.switchToWindowUrl('http://http://52.11.126.234/app.php/staff/indicator/print/id/'+ id +'')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/1.Indicadores/General/1.Index/8.Print/4.btn_Print'))

