import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('1.Indicadores/Daruma/Acceso al modulo'), [:])

WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/General/1.Index/8.Print/2.btn_Indicator_Options'))

WebUI.click(findTestObject('1.Indicadores/General/1.Index/9.Import/2.a_Import'))

WebUI.delay(5)

CustomKeywords.'test.Metodos.uploadFile'('$(\'#ap_import_name\').click()', 'C:\\Users\\Jessica\\Desktop\\FormatToImport.csv')

WebUI.executeJavaScript('$(\"#tq_form_submit\").click()', null)

