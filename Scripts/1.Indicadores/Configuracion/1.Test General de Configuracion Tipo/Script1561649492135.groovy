import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//Ver Tipos
WebUI.callTestCase(findTestCase('1.Indicadores/Daruma/Acceso al modulo'), [:])

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/0.Ver/1.a_Configuration'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/0.Ver/2.a_Types'))

WebUI.delay(5)

//Crear Tipos
WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/1.Crear/1.btn_Create'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/1.Crear/2.input_typename'), 'Prueba DEMO')

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/1.Crear/3.inputCheck_selectAll'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/1.Crear/3.inputCheck_selectAll'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/1.Crear/4.inputCheck_notes_required'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/1.Crear/5.textarea_Description'), 'Esto es una prueba par Katalon.')

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/1.Crear/6.btn_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Tipo'))

//Editar Tipo
WebUI.delay(5)

//WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/1.inputCheck_items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/2.btn_Edit'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/3.input_typename'), 'Edicion DEMO')

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/4.inputCheck_DeselectAll'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/5.inputCheck_DeselectAll'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/6.inputCheck_notes_required'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/7.textarea_Description'), 'Estoy editando un registro')

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/2.Editar/8.btn_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Tipo'))

//Eliminar Tipo
WebUI.delay(5)

//WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/3.Eliminar/1.inputCheck_Items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/3.Eliminar/2.btn_Delete'))

WebUI.acceptAlert()

//Inhabilitar Tipo
WebUI.delay(5)

//WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/4.Inhabilitar/1.inputCheck_Items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/4.Inhabilitar/2.btn_Disable_1'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/4.Inhabilitar/3.btn_Disable_2'))

WebUI.acceptAlert()

WebUI.delay(5)

//Filtrar
WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/5.Filtrar/1.btn_Open_Filter'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/5.Filtrar/2.input_filtersnametext'), 'Calidad')

WebUI.setText(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/5.Filtrar/3.input_filtersabbrtext'), 'C')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/5.Filtrar/4.select_filters_state'),
	'1', true)

WebUI.setText(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/5.Filtrar/5.input_filtersnotestext'), 'Esto es una prueba par Katalon.')

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/5.Filtrar/6.btn_Filter'))

WebUI.delay(5)

WebUI.click(findTestObject('1.Indicadores/Configuracion/1.Tipos/1.Generales/5.Filtrar/1.btn_Open_Filter'))

WebUI.click(findTestObject('Object Repository/1.Indicadores/Configuracion/1.Tipos/1.Generales/5.Filtrar/8.btn_reset'))
