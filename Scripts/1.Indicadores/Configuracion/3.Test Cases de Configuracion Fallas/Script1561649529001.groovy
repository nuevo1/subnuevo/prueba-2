import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('1.Indicadores/Daruma/Acceso al modulo'), [:])
//Ver Fallas
WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/0.Ver/1.a_Configuration'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/0.Ver/2.a_Fail'))

WebUI.delay(5)

//Crear Fallas
WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/1.Crear/1.btn_Create'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/1.Crear/2.input_failname'), 'PRUEBA DEMO')

WebUI.setText(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/1.Crear/3.textarea_Description'), 'Estamos creando un registro')

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/1.Crear/4.inputCheck_List'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/1.Crear/5.inputCheck_List'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/1.Crear/6.btn_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Fallas'))

//Editar Fallas
WebUI.delay(5)

//WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/2.Editar/1.inputCheck_Items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck')) 

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/2.Editar/2.btn_Edit'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/2.Editar/3.input_failname'), 'Edicion de la Prueba')

WebUI.setText(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/2.Editar/4.textarea_Description'), 'Estamos Editando un registro')

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/2.Editar/5.inputCheck_List'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/2.Editar/6.btn_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/1.Indicadores/TestCase General/Volver_Fallas'))

//Eliminar Fallas
WebUI.delay(5)

//WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/3.Eliminar/1.inputCheck_Items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/3.Eliminar/2.btn_Delete'))

WebUI.acceptAlert()

//Inhabilitar Fallas
WebUI.delay(5)

//WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Alto/4.Inhabilitar/1.inputCheck_Items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/4.Inhabilitar/2.btn_Disable_1'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/4.Inhabilitar/3.btn_Disable_2'))

WebUI.acceptAlert()

WebUI.delay(5)

//Filtrar
WebUI.click(findTestObject('Object Repository/1.Indicadores/Configuracion/3.Fallas/1.Generales/5.Filtrar/1.btn_Open_Filter'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/5.Filtrar/2.input_filtersnametext'), 'Edicion de la Prueba')

WebUI.setText(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/5.Filtrar/3.input_filtersabbrtext'), 'EDLP')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/5.Filtrar/4.select_filters_state'),
	'1', true)

WebUI.setText(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/5.Filtrar/5.input_filtersnotestext'), 'Estamos Editando un registro')

WebUI.click(findTestObject('1.Indicadores/Configuracion/3.Fallas/1.Generales/5.Filtrar/6.btn_filter'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/1.Indicadores/Configuracion/3.Fallas/1.Generales/5.Filtrar/1.btn_Open_Filter'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/1.Indicadores/Configuracion/3.Fallas/1.Generales/5.Filtrar/8.btn_reset'))
