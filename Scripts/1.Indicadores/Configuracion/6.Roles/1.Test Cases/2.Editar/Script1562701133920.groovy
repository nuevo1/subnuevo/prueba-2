import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('1.Indicadores/Configuracion/6.Roles/1.Test Cases/0.Ver'), [:])

WebUI.delay(5)

//Editar Roles
//WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Generales/2.Editar/1.inputCheck_Items'))
CustomKeywords.'test.Metodos.lista'(findTestObject('Object Repository/2.list_inputCheck'))

WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Generales/2.Editar/2.btn_Edit'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Generales/2.Editar/3.input_rolename'), 'Edicion prueba...')

WebUI.selectOptionByValue(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Generales/2.Editar/4.select_role_type'), '4',
	true)

WebUI.setText(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Generales/2.Editar/5.textarea_Description'), 'Esto es una edicion de Katalon')

WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Generales/2.Editar/6.btn_Save'))

WebUI.delay(5)