import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String palabra = CustomKeywords.'test.Metodos.randomString'()

WebUI.callTestCase(findTestCase('1.Indicadores/Configuracion/6.Roles/1.Test Cases/0.Ver'), [:])

//Crear Roles
WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Generales/1.Crear/1.btn_Create'))

WebUI.setText(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Generales/1.Crear/2.input_rolename'), palabra)

WebUI.selectOptionByValue(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Generales/1.Crear/3.select_role_type'), '3',
	true)

WebUI.setText(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Generales/1.Crear/4.textarea_Description'), 'Esto es una prueba de Katalon')

WebUI.click(findTestObject('1.Indicadores/Configuracion/6.Roles/1.Generales/1.Crear/5.btn_Save'))

WebUI.delay(5)
