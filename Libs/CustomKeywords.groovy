
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import com.kms.katalon.core.testobject.TestObject

import java.lang.String


def static "test.Metodos.select2"(
    	TestObject objeto	
     , 	int sel	) {
    (new test.Metodos()).select2(
        	objeto
         , 	sel)
}

def static "test.Metodos.tiny1"(
    	TestObject obj1	
     , 	int num	) {
    (new test.Metodos()).tiny1(
        	obj1
         , 	num)
}

def static "test.Metodos.EdiTtiny1"(
    	TestObject obj1	
     , 	int num	) {
    (new test.Metodos()).EdiTtiny1(
        	obj1
         , 	num)
}

def static "test.Metodos.tinyFormula"(
    	TestObject obj2	) {
    (new test.Metodos()).tinyFormula(
        	obj2)
}

def static "test.Metodos.randomString"() {
    (new test.Metodos()).randomString()
}

def static "test.Metodos.randomStringEdit"() {
    (new test.Metodos()).randomStringEdit()
}

def static "test.Metodos.uploadFile"(
    	String to	
     , 	String filePath	) {
    (new test.Metodos()).uploadFile(
        	to
         , 	filePath)
}

def static "test.Metodos.display_list_only"(
    	TestObject list	
     , 	int num	) {
    (new test.Metodos()).display_list_only(
        	list
         , 	num)
}

def static "test.Metodos.display_list_multi"(
    	TestObject list	
     , 	int num	) {
    (new test.Metodos()).display_list_multi(
        	list
         , 	num)
}

def static "test.Metodos.list_Action"(
    	TestObject obj	
     , 	int index	) {
    (new test.Metodos()).list_Action(
        	obj
         , 	index)
}

def static "test.Metodos.element_selected"(
    	TestObject ele	
     , 	int num	) {
    (new test.Metodos()).element_selected(
        	ele
         , 	num)
}

def static "test.Metodos.lista"(
    	TestObject obj	) {
    (new test.Metodos()).lista(
        	obj)
}

def static "test.Metodos.select_item_managment"(
    	TestObject lista_href	
     , 	int id	) {
    (new test.Metodos()).select_item_managment(
        	lista_href
         , 	id)
}

def static "test.Metodos.select2_config"(
    	TestObject obj	) {
    (new test.Metodos()).select2_config(
        	obj)
}

def static "test.Metodos.ValidarFiltros"(
    	String abrr	
     , 	TestObject abr	) {
    (new test.Metodos()).ValidarFiltros(
        	abrr
         , 	abr)
}

def static "test.Metodos.isNumeric"(
    	String cadena	) {
    (new test.Metodos()).isNumeric(
        	cadena)
}

def static "test.Metodos.list_report"(
    	TestObject obj	) {
    (new test.Metodos()).list_report(
        	obj)
}

def static "test.Metodos.validation"(
    	TestObject obj	
     , 	int val	) {
    (new test.Metodos()).validation(
        	obj
         , 	val)
}

def static "test.Metodos.select2_multiselect"(
    	TestObject obj	
     , 	int num	) {
    (new test.Metodos()).select2_multiselect(
        	obj
         , 	num)
}

def static "test.Metodos.get_numbers"(
    	TestObject obj	) {
    (new test.Metodos()).get_numbers(
        	obj)
}

def static "test.Metodos.select_items_actions"(
    	String css	) {
    (new test.Metodos()).select_items_actions(
        	css)
}

def static "test.Metodos.selectItem_taskState"(
    	TestObject obj	) {
    (new test.Metodos()).selectItem_taskState(
        	obj)
}

def static "test.Metodos.select2_multiselect1"(
    	TestObject obj	
     , 	int num	) {
    (new test.Metodos()).select2_multiselect1(
        	obj
         , 	num)
}

def static "test.Metodos.get_Text"(
    	TestObject obj	) {
    (new test.Metodos()).get_Text(
        	obj)
}

def static "test.Metodos.selectA"(
    	TestObject obj	) {
    (new test.Metodos()).selectA(
        	obj)
}

def static "test.Metodos.display_list_multi2"(
    	TestObject list	
     , 	int num	) {
    (new test.Metodos()).display_list_multi2(
        	list
         , 	num)
}

def static "test.Metodos.get_numbers1"(
    	TestObject obj	) {
    (new test.Metodos()).get_numbers1(
        	obj)
}

def static "test.Metodos.IndentificacionAleatoria"() {
    (new test.Metodos()).IndentificacionAleatoria()
}

def static "test.Metodos.randomSelect"(
    	TestObject objeto	) {
    (new test.Metodos()).randomSelect(
        	objeto)
}

def static "test.Metodos.get_text"(
    	TestObject obj	) {
    (new test.Metodos()).get_text(
        	obj)
}

def static "test.Connection.ConnctDB"(
    	String url	
     , 	String DBName	
     , 	String port	
     , 	String userName	
     , 	String password	) {
    (new test.Connection()).ConnctDB(
        	url
         , 	DBName
         , 	port
         , 	userName
         , 	password)
}

def static "test.Connection.executeQuery"(
    	String queryString	) {
    (new test.Connection()).executeQuery(
        	queryString)
}
