import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\JHONSA~1\\AppData\\Local\\Temp\\Katalon\\Test Cases\\2.Planes de Accion\\Configuracion\\4.Priorizacion de Causas\\1.Test Cases General\\3.Eliminar\\20191018_080125\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCase('Test Cases/2.Planes de Accion/Configuracion/4.Priorizacion de Causas/1.Test Cases General/3.Eliminar', new TestCaseBinding('Test Cases/2.Planes de Accion/Configuracion/4.Priorizacion de Causas/1.Test Cases General/3.Eliminar',[:]), FailureHandling.STOP_ON_FAILURE , false)
    
